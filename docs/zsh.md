# ZSH

## shortcuts

| shortcut   | description                       |
| ---------- | --------------------------------- |
| `ctrl + r` | search history                    |
| `ctrl + w` | delete word                       |
| `ctrl + u` | delete line                       |
| `ctrl + a` | go to beginning of line           |
| `ctrl + e` | go to end of line                 |
| `ctrl + k` | delete from cursor to end of line |
| `ctrl + l` | clear screen                      |
| `ctrl + c` | kill current command              |
| `ctrl + d` | exit current shell                |
| `ctrl + t` | select file                       |

## aliases

| alias     | command    |
| --------- | ---------- |
| `..`      | `cd ..`    |
| `...`     | `cd ../..` |

## functions

| function   | command                |
| -----------| ---------------------- |
| `mkcd`     | `mkdir -p $1 && cd $1` |

## useful plugins

| plugin                  | description                                           |
| ----------------------- | ----------------------------------------------------- |
| zsh-autosuggestions     | suggests commands as you type                         |
| zsh-syntax-highlighting | highlights commands as you type                       |
| zsh-completions         | adds completion for many commands                     |
| zsh-notify              | shows notifications when long commands finish         |
| zsh-quickstart          | adds a quickstart menu                                |
| zsh-interactive-cd      | adds a `cd` command that can autocomplete directories |