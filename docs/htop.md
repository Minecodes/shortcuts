# HTOP
> htop - an interactive process viewer

## arguments

| argument | description |
| --- | --- |
| `-u`, `--user <username>` | Show process of a user |
| `-p`, `--pid <pid>` | Show process of a pid |
| `-s`, `--sort-key <key>` | Sort by key |
| `-d`, `--delay <delay>` | Delay between updates |
| `-C`, `--no-color` | Disable colors |

## keys

| key | description |
| --- | --- |
| `F1` | Help |
| `F2` | Setup |
| `F3` | Search |
| `F4` | Filter |
| `F5` | Tree |
| `F6` | Sort |
| `F9` | Kill |
| `F10` | Quit |

More: [htop-dev/htop](https://github.com/htop-dev/htop)