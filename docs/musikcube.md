# MUSIKCUBE
> a cross-platform, terminal-based music player, audio engine, metadata indexer, and server in c++ 

## hotkeys
### global
|       key       |             action            |
| --------------- | ----------------------------- |
| `ESC`           | focus/defocus the command bar |
| `TAB`           | select next window            |
| `SHIFT+TAB`     | select previous window        |
| `^D`            | quit                          |

### main view

|       key       |             action            |
| --------------- | ----------------------------- |
| `~`             | switch to console view        |
| `a`             | switch to library view        |
| `s`             | switch to settings view       |

### playback

|       key       |             action            |
| --------------- | ----------------------------- |
| `SPACE`         | toggle play/pause             |
| `u`             | seek back 10 seconds          |
| `o`             | seek forward 10 seconds       |
| `i`             | volume up by 5%               |
| `k`             | volume down by 5%             |
| `j`             | previous track                |
| `l`             | next track                    |
| `m`             | toggle mute                   |
| `.`             | toggle repeat                 |
| `,`             | toggle shuffle                |
| `^p`            | toggle play/pause (globally)  |
| `^x`            | stop                          |

More: [clangen/musikcube](https://github.com/clangen/musikcube/wiki/user-guide)