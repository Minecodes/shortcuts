# VMCHAMP
> Simple and fast creation of throwaway VMs on your local machine. Connect via SSH in just a few seconds.

## commands
### `vmchamp run` or `vmchamp start`
> Run a VM with the given name. If the VM does not exist, it will be created.

**Usage:** `vmchamp run [<name>] [options]`
> Default name: testvm

#### options
|             option            |                         action                        |
| ----------------------------- | ----------------------------------------------------- |
| `--os`                        | Selects the os image [default: Debian11]              |
| `--mem`                       | Sets the memory size [default: 256MiB]                |
| `--disk`                      | Sets the disk size [default: 4GiB]                    |
| `--cpu`                       | Sets the cpu count [default: 1]                       |
| `--bg`                        | Run the VM in the background                          |
| `--local-image <local-image>` | Use a local image instead of downloading one          |
| `--cmd`                       | Run a command in the VM                               |

### `vmchamp reboot`, `vmchamp restart` or `vmchamp reset`
> Reboot a VM with the given name.

**Usage:** `vmchamp reboot [<name>]`

### `vmchamp clean` or `vmchamp purge`
> Deletes all VMs and images.

**Usage:** `vmchamp clean`

### `vmchamp rm` or `vmchamp remove`
> Deletes a VM with the given name.

**Usage:** `vmchamp rm [<name>]`

### `vmchamp ls`, `vmchamp ps` or `vmchamp list`
> Lists all VMs.

**Usage:** `vmchamp ls`

### `vmchamp ssh`
> Connects to a VM with the given name via SSH.

**Usage:** `vmchamp ssh [<name>]`

### `vmchamp images` or `vmchamp os`
> Lists all available images.

**Usage:** `vmchamp images`


More: [wubbl0rz/VmChamp](https://github.com/wubbl0rz/VmChamp)